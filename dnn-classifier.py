import numpy as np
import pandas as pd
import tensorflow as tf

TEST_CATEGORIES = "TEST_CATEGORIES"
TEST_DATA = "TEST_DATA"
TEST_FEATURES = "TEST_FEATURES"

TRAINING_CATEGORIES = "TRAINING_CATEGORIES"
TRAINING_DATA = "TRAINING_DATA"
TRAINING_FEATURES = "TRAINING_FEATURES"


def fetch_data_from_csvs() -> dict:
    """
    Fetch and parse training and test data from separate CSV files using Pandas
    :return: a dictionary containing the training and test data sets
    """
    training_data = pd.read_csv("training-data.csv", header=None).values
    test_data = pd.read_csv("test-data.csv", header=None).values

    return {
        TRAINING_DATA: training_data,
        TEST_DATA: test_data,
    }


def separate_features_and_categories(training_and_test_data: dict) -> dict:
    """
    Separate features and categories from each of the training and test data sets
    :param training_and_test_data: dictionary containing the training and test data sets
    :return: a new dictionary containing features and categories for each of the training and test data sets
    """
    return {
        TRAINING_FEATURES: format_features(training_and_test_data[TRAINING_DATA][:, :-1]),
        TRAINING_CATEGORIES: training_and_test_data[TRAINING_DATA][:, -1:],
        TEST_FEATURES: format_features(training_and_test_data[TEST_DATA][:, :-1]),
        TEST_CATEGORIES: training_and_test_data[TEST_DATA][:, -1:],
    }


def format_features(features: np.ndarray) -> np.ndarray:
    """
    format features for TensorFlow compatibility
    :param features: Numpy array of features to be reformatted
    :return: Numpy array of formatted features
    """
    formatted_features = dict()
    num_columns = features.shape[1]

    for i in range(0, num_columns):
        formatted_features[str(i)] = features[:, i]

    return formatted_features


def define_feature_columns(training_features: np.ndarray) -> list:
    """
    specify feature columns for the TensorFlow classifier
    :param training_features: Numpy array of features in which to infer feature columns
    :return: a list of the feature columns
    """
    feature_columns = []

    for key in training_features.keys():
        feature_columns.append(tf.feature_column.numeric_column(key=key))

    return feature_columns


def instantiate_dnn_classifier(feature_columns: list) -> tf.estimator.DNNClassifier:
    """
    define a TensorFlow DNN classifier given feature columns
    :param feature_columns: the feature columns from the training data set features
    :return: an instantiation of the TensorFlow DNN classifier
    """
    classifier = tf.estimator.DNNClassifier(
        feature_columns=feature_columns,
        hidden_units=[20, 30, 20],
        n_classes=10
    )

    return classifier


def train_fn(features: list, labels: list, batch_size: int) -> tf.data.Dataset:
    """
    training function for the TensorFlow classifier
    :param features: features from the training data set
    :param labels: categories from the training data set
    :param batch_size: the number of features and categories to provide to the classifier at once
    :return: a TensorFlow training data set for the classifier
    """
    return tf.data.Dataset.from_tensor_slices((dict(features), labels)).shuffle(1000).repeat().batch(batch_size)


def test_fn(features: list, labels: list, batch_size: int) -> tf.data.Dataset:
    """
    test function for the TensorFlow classifier
    :param features: features from the test data set
    :param labels: categories from the test data set
    :param batch_size: the number of features and categories to provide to the classifier at once
    :return: a TensorFlow test data set for the classifier
    """
    return tf.data.Dataset.from_tensor_slices((dict(features), labels)).batch(batch_size)


def train_classifier(classifier: tf.estimator.DNNClassifier, features_and_categories: dict) -> None:
    """
    train the classifier
    :param classifier: classifier to train
    :param features_and_categories: features and categories from both the training and test data sets
    :return: nothing
    """
    classifier.train(
        input_fn=lambda: train_fn(features_and_categories[TRAINING_FEATURES],
                                  features_and_categories[TRAINING_CATEGORIES],
                                  100),
        steps=50
    )


def test_classifier(classifier: tf.estimator.DNNClassifier, features_and_categories: dict) -> dict:
    """
    test the classifier
    :param classifier: classifier to test
    :param features_and_categories: features and categories from both the training and test data sets
    :return: metrics from the classifier's testing, including the accuracy rate
    """
    return classifier.evaluate(
        input_fn=lambda: test_fn(features_and_categories[TEST_FEATURES], features_and_categories[TEST_CATEGORIES], 100)
    )


def main():
    training_and_test_data = fetch_data_from_csvs()
    features_and_categories = separate_features_and_categories(training_and_test_data)
    feature_columns = define_feature_columns(features_and_categories[TRAINING_FEATURES])
    classifier = instantiate_dnn_classifier(feature_columns)
    train_classifier(classifier, features_and_categories)
    metrics = test_classifier(classifier, features_and_categories)
    print(f"Test accuracy: {np.round([metrics['accuracy']], 3)}")


if __name__ == "__main__":
    main()
